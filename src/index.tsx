import React from 'react'
import {render} from 'react-dom'
import dotenv from 'dotenv'

import store from "./app/store";
import {Provider} from "react-redux";
import App from "./app/App";
import "./scss/main.scss"

dotenv.config()

const mainElement = document.createElement('div')
mainElement.setAttribute('id', 'root')
document.body.appendChild(mainElement)

const Index = () => {
  return (
    <>
      <Provider store={store}>
        <App/>
      </Provider>
    </>
  )
}

render(<Index/>, mainElement)
