import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
  FBinanceLoading,
  fetchFBinanceAccountInfo,
  fetchFBinanceOpenOrders,
  fetchFBinanceServerTime,
  fetchFBinanceTransactions,
} from "../service/features/fBinanceSlice";
import {RootState} from "../app/rootReducer";
import FBinanceBalance from "../components/FBinanceBalance";
import FBinancePositions from "../components/FBinancePositions";
import FBinanceOrders from "../components/FBinanceOrders";
import {FBIncomeType, FBSymbolTickerStream} from "../service/model/fBinance";
import FBinanceTransactions from "../components/FBinanceTransactions";
import moment from "moment";
import {Container} from "react-bootstrap";
import {FBinanceWSMsg, FBinanceWSMsgMethod, sendFBinanceSocketMsg} from "../app/middlewares/fBinanceMarketWSMiddleware";
import FBinanceTicker from "../components/FBinanceTicker";

const FBinanceDashboard = () => {
  const dispatch = useDispatch()
  const {account, transactions, openOrders, isLoading, serverTimeDiff} = useSelector((state: RootState) => state.fBinance)
  const {streams} = useSelector((state: RootState) => state.wsFBinance)
  
  const [ticker, setTicker] = useState<Array<FBSymbolTickerStream>>([])
  
  const fetchTransactions = () => {
    const startTime = moment().startOf('month').add(6, 'hours') .valueOf()
    dispatch(fetchFBinanceTransactions(serverTimeDiff, FBIncomeType.REALIZED_PNL, startTime))
  }
  
  const fetchAccount = () => dispatch(fetchFBinanceAccountInfo(serverTimeDiff))
  const fetchOpenOrders = () => dispatch(fetchFBinanceOpenOrders(serverTimeDiff))
  
  const subscribeAllTickerStream = (unsubscribe?: boolean) => {
    const wsMsg: FBinanceWSMsg = {
      method: unsubscribe ? FBinanceWSMsgMethod.UNSUBSCRIBE : FBinanceWSMsgMethod.SUBSCRIBE,
      params: ['!ticker@arr'],
      id: 12
    }
    dispatch(sendFBinanceSocketMsg(wsMsg))
  }
  
  useEffect(() => {
    subscribeAllTickerStream()
    dispatch(fetchFBinanceServerTime())
    return () => {
      subscribeAllTickerStream(true)
    }
  }, [])
  
  useEffect(() => {
    // @ts-ignore
    if (!streams || !Array.isArray(streams['!ticker@arr'])) return
    // @ts-ignore
    const newTicker = [...streams['!ticker@arr']]
    const symbols = new Set(newTicker.map(e => e.s))
    const updateTicker = newTicker
      .concat([...ticker].filter(e => !symbols.has(e.s)))
      .sort((a, b) => a.s.localeCompare(b.s))
    setTicker(updateTicker)
    // @ts-ignore
  }, [streams['!ticker@arr']])
  
  useEffect(() => {
    if (serverTimeDiff) {
      fetchAccount()
      fetchOpenOrders()
      fetchTransactions()
    }
  }, [serverTimeDiff])
  
  const refreshBalanceAndOrders = () => {
    fetchAccount()
    fetchOpenOrders()
  }
  
  return (
    <>
      <Container fluid>
        <FBinanceTicker ticker={ticker}/>
        <FBinanceBalance account={account} isLoading={isLoading[FBinanceLoading.account]}
                         refreshFunction={refreshBalanceAndOrders}/>
        <FBinancePositions account={account} orders={openOrders} ticker={ticker} isLoading={isLoading[FBinanceLoading.account]}/>
        <FBinanceOrders orders={openOrders} ticker={ticker} isLoading={isLoading[FBinanceLoading.openOrders]}
                        refreshFunction={fetchOpenOrders}/>
        <FBinanceTransactions transactions={transactions} isLoading={isLoading[FBinanceLoading.transactions]}
                              refreshFunction={fetchTransactions}/>
      </Container>
    </>
  )
}


export default FBinanceDashboard
