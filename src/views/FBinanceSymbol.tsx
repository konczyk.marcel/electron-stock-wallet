import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {Container} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../app/rootReducer";
import moment from "moment";
import {FBinanceLoading, fetchFBinanceUserTrades} from "../service/features/fBinanceSlice";
import {FBinanceWSMsg, FBinanceWSMsgMethod, sendFBinanceSocketMsg} from "../app/middlewares/fBinanceMarketWSMiddleware";
import FBinanceTrades from "../components/FBinanceTrades";
import {FBSymbolTickerStream} from "../service/model/fBinance";

const FBinanceSymbol = () => {
  const {symbol} = useParams()
  const dispatch = useDispatch()
  const {trades, isLoading, serverTimeDiff} = useSelector((state: RootState) => state.fBinance)
  const {streams} = useSelector((state: RootState) => state.wsFBinance)
  
  const [ticker, setTicker] = useState<FBSymbolTickerStream | null>(null)
  
  const fetchTrades = () => {
    const startTime = moment().startOf("month").add("6", "hours").valueOf()
    dispatch(fetchFBinanceUserTrades(serverTimeDiff, symbol, startTime))
  }
  
  useEffect(() => {
    // @ts-ignore
    setTicker(streams[`${symbol}@ticker`])
    // @ts-ignore
  }, [streams[`${symbol}@ticker`]])
  
  const subscribeSymbolTickerStream = (unsubscribe?: boolean) => {
    const newStream = symbol.concat('@ticker')
    const wsMsg: FBinanceWSMsg = {
      method: unsubscribe ? FBinanceWSMsgMethod.UNSUBSCRIBE : FBinanceWSMsgMethod.SUBSCRIBE,
      params: [newStream],
      id: 1234
    }
    dispatch(sendFBinanceSocketMsg(wsMsg))
  }
  
  useEffect(() => {
    subscribeSymbolTickerStream()
    fetchTrades()
    return () => {
      subscribeSymbolTickerStream(true)
    }
  }, [])
  
  return (
    <>
      <Container fluid>
        <h1>{symbol} - {ticker?.c}</h1>
        <FBinanceTrades trades={trades} isLoading={isLoading[FBinanceLoading.trades]} refreshFunction={fetchTrades}/>
      </Container>
    </>
  )
}

export default FBinanceSymbol
