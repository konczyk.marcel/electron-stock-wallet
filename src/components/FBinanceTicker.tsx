import React, {useEffect, useState} from "react";
import {FBSymbolTickerStream} from "../service/model/fBinance";
import {Form, Table} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {useDispatch, useSelector} from "react-redux";
import {setSelectedSymbol} from "../service/features/fbDashboardSlice";
import {RootState} from "../app/rootReducer";

interface FBinancePositionsProps {
  ticker: Array<FBSymbolTickerStream>
}

const FBinanceTicker = ({ticker}: FBinancePositionsProps) => {
  const dispatch = useDispatch()
  const [shownSymbols, setShownSymbol] = useState<Map<string, boolean>>(new Map())
  const [isEditMode, setIsEditMode] = useState<boolean>(false)
  
  const handleChange = (evt: any, symbol: string) => setShownSymbol(shownSymbols.set(symbol, evt.target.checked))
  const handleSymbolClick = (symbol: string) => dispatch(setSelectedSymbol(symbol))
  const {selectedSymbol} = useSelector((state: RootState) => state.fbDashboard)
  
  const storeShownSymbols = () => localStorage.setItem('fb_ticker_shownSymbols', JSON.stringify(Array.from(shownSymbols)))
  const getShownSymbols = () => {
    const storedShownSymbols = localStorage.getItem('fb_ticker_shownSymbols')
    if (storedShownSymbols) setShownSymbol(new Map(JSON.parse(storedShownSymbols)))
  }
  
  useEffect(() => {
    if (shownSymbols.size > 0) storeShownSymbols()
  }, [isEditMode])
  
  useEffect(() => {
    getShownSymbols()
  }, [])
  
  const tickerTemp = (ticker.length > 0 && shownSymbols.size > 0) || isEditMode ?
    <>
      <Table striped bordered size="sm">
        <thead>
        <tr>
          {isEditMode ? <th key="action">Visible</th> : <></>}
          <th key="symbol">Symbol</th>
          <th key="last">Last</th>
          <th key="chg">Chg</th>
          <th key="chgp">Chg%</th>
        </tr>
        </thead>
        <tbody>
        {ticker.filter(e => shownSymbols.get(e.s) || isEditMode).map((e: FBSymbolTickerStream) =>
          <tr key={e.s}>
            {isEditMode ? <td><Form.Check type="checkbox" checked={shownSymbols.get(e.s)} onChange={(evt: any) => handleChange(evt, e.s)}/></td> : <></>}
            <td style={{cursor: "pointer"}} onClick={() => handleSymbolClick(e.s)}>{e.s} {e.s === selectedSymbol ? '(x)' : ''}</td>
            <td >{e.c}</td>
            <td className={+e.p >= 0 ? 'text-green' : 'text-red'}>{e.p}</td>
            <td className={+e.P >= 0 ? 'text-green' : 'text-red'}>{(+e.P).toFixed(2)}%</td>
          </tr>)}
        </tbody>
      </Table>
    </> : "No active tickers."
  
  return (
    <>
      <div style={{display: "flex", alignItems: "center"}}>
        <h1>Ticker</h1>
        <FontAwesomeIcon icon={faEdit} onClick={() => setIsEditMode(!isEditMode)}
                         style={{marginLeft: '5px', cursor: "pointer"}}/>
      </div>
      {tickerTemp}
    </>
  )
}

export default FBinanceTicker
