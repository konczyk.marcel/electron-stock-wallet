import {Button, Form, Modal} from "react-bootstrap";
import OrderInput, {OrderInputSide, OrderInputTimeInForce, OrderInputType} from "../../service/model/binance/OrderInput";
import React, {useEffect, useState} from "react";
import {FBLeverage, FBSymbolTickerStream} from "../../service/model/fBinance";
import {createNewFutureOrder, updateInitialLeverage} from "../../service/api/fBinanceApi";
import {useSelector} from "react-redux";
import {RootState} from "../../app/rootReducer";

interface FBinanceOrderModalProps {
  showModal: boolean,
  ticker: Array<FBSymbolTickerStream>,
  onSubmit: CallableFunction,
  cancelModalHandler: CallableFunction
}

const FBinanceOrderModal = ({showModal, ticker, onSubmit, cancelModalHandler}: FBinanceOrderModalProps) => {
  const {serverTimeDiff, account} = useSelector((state: RootState) => state.fBinance)
  const {selectedSymbol} = useSelector((state: RootState) => state.fbDashboard)
  const initialOrder: OrderInput = {
    symbol: selectedSymbol,
    side: OrderInputSide.BUY,
    type: OrderInputType.MARKET
  }
  
  const [newOrder, setNewOrder] = useState<OrderInput>(initialOrder)
  const [initialLeverage, setInitialLeverage] = useState<FBLeverage>()
  const [isLeverageLoading, setIsLeverageLoading] = useState<boolean>(false)
  const [isFormValid, setIsFormValid] = useState<boolean>(false)
  
  const handleNewOrderChange = (e: HTMLFormElement) => {
    const updatedOrder = {...newOrder}
    // @ts-ignore
    updatedOrder[e.name] = e.value
    // @ts-ignore
    setNewOrder(updatedOrder)
  }
  
  useEffect(() => {
    const {symbol, ...rest} = newOrder
    setNewOrder({symbol: selectedSymbol, ...rest})
  }, [selectedSymbol])
  
  useEffect(() => {
    validateForm()
  }, [newOrder, initialLeverage])
  
  useEffect(() => {
    const {symbol, side, type, quantity} = newOrder
    let o: OrderInput = {symbol, side, type, quantity}
    if (newOrder.type === OrderInputType.LIMIT) o.timeInForce = OrderInputTimeInForce.GTC
    setNewOrder(o)
  }, [newOrder.type])
  
  const submitNewOrder = async () => {
    const payload = {...newOrder}
    if (newOrder.type === OrderInputType.LIMIT) {
      payload.timeInForce = OrderInputTimeInForce.GTC
    }
    const res = await createNewFutureOrder(serverTimeDiff, newOrder)
    if (res) {
      onSubmit()
    }
  }
  
  const [newInitialLeverage, setNewInitialLeverage] = useState<number>(10)
  
  const handleNewLeverage = () => {
    postInitialLeverage(selectedSymbol, newInitialLeverage)
  }
  
  useEffect(() => {
    if (showModal) postInitialLeverage()
  }, [showModal])
  
  const postInitialLeverage = (symbol?: string, leverage?: number) => {
    setIsLeverageLoading(true)
    updateInitialLeverage(serverTimeDiff, symbol || selectedSymbol, leverage || 10)
      .then(res => {
        setInitialLeverage(res)
        setIsLeverageLoading(false)
      })
      .catch(() => setIsLeverageLoading(false))
  }
  
  const calculateMaxOrderAmount = (): number => {
    const symbolTicker = ticker?.find(e => e.s === selectedSymbol)
    const price = newOrder.type == OrderInputType.MARKET ? +(symbolTicker?.c || 0) : (newOrder?.price || 0)
    const res = (+(account?.availableBalance || 0) * (initialLeverage?.leverage || 0)) / price
    return Math.floor(res * 1000) / 1000
  }
  
  const validateForm = () => {
    switch(true) {
      case (!newInitialLeverage || newInitialLeverage <= 0 || newInitialLeverage > 125): setIsFormValid(false); break;
      case (!newOrder.symbol || !newOrder.type || !newOrder.side): setIsFormValid(false); break;
      case (!newOrder.quantity || +newOrder.quantity <= 0): setIsFormValid(false); break;
      case (newOrder.type === OrderInputType.LIMIT && (!newOrder.price || +newOrder.price <= 0)): setIsFormValid(false); break;
      default: setIsFormValid(true)
    }
  }
  
  return (
    <Modal show={showModal} onHide={cancelModalHandler}>
      <Modal.Header closeButton><Modal.Title>New Order - {selectedSymbol}</Modal.Title></Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="newLeverage">
            <Form.Label>Leverage</Form.Label>
            <Form.Control onChange={(e: any) => setNewInitialLeverage(e.target.value)}
                          onBlur={() => handleNewLeverage()} type="number" name="initialLeverage"
                          disabled={isLeverageLoading} value={newInitialLeverage} min="1" max="20"/>
            <Form.Text className="text-muted">
              curr. leverage {isLeverageLoading ? "Loading ..." : initialLeverage?.leverage}
              , max. value {isLeverageLoading ? "Loading ..." : initialLeverage?.maxNotionalValue}
            </Form.Text>
          </Form.Group>
        </Form>
        <Form onChange={(e: any) => handleNewOrderChange(e.target)}>
          <Form.Group controlId="newOrder.Side">
            <Form.Label>Side</Form.Label>
            <Form.Control as="select" name="side" value={newOrder.side} onChange={() => {}}>
              <option>{OrderInputSide.BUY}</option>
              <option>{OrderInputSide.SELL}</option>
            </Form.Control>
          </Form.Group>
          <Form.Group controlId="newOrder.Type">
            <Form.Label>Type</Form.Label>
            <Form.Control as="select" name="type" value={newOrder.type} onChange={() => {}}>
              <option>{OrderInputType.MARKET}</option>
              <option>{OrderInputType.LIMIT}</option>
            </Form.Control>
            <Form.Text className="text-muted">Ticker: {ticker?.find(e => e.s === selectedSymbol)?.c || 'Not streaming'}</Form.Text>
          </Form.Group>
          {newOrder?.type === OrderInputType.LIMIT ?
            <Form.Group controlId="newOrder.Price">
              <Form.Label>Price</Form.Label>
              <Form.Control type="number" name="price" value={newOrder.price} min="0" onChange={() => {}}/>
            </Form.Group>
            : ''}
          <Form.Group controlId="newOrder.Amount">
            <Form.Label>Amount</Form.Label>
            <Form.Control value={newOrder.quantity} name="quantity" type="number" min="0" placeholder="Amount" onChange={() => {}}/>
            <Form.Text className="text-muted">avlb. {(+(account?.availableBalance || 0)).toFixed(2)},
              max. {calculateMaxOrderAmount()}</Form.Text>
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => cancelModalHandler()} size="sm">Close</Button>
        <Button variant="success" onClick={submitNewOrder} disabled={!isFormValid} size="sm">Create</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default FBinanceOrderModal
