import {Button, Form, Modal} from "react-bootstrap";
import OrderInput, {OrderInputSide, OrderInputTimeInForce, OrderInputType} from "../../service/model/binance/OrderInput";
import React, {useEffect, useState} from "react";
import {FBLeverage, FBOrder, FBSymbolTickerStream} from "../../service/model/fBinance";
import {cancelFutureOrder, createNewFutureOrder, updateInitialLeverage} from "../../service/api/fBinanceApi";
import {useSelector} from "react-redux";
import {RootState} from "../../app/rootReducer";
import OrderCancelInput from "../../service/model/binance/OrderCancelInput";

interface FBinanceOrderModalProps {
  showModal: boolean,
  order: FBOrder | null,
  ticker: Array<FBSymbolTickerStream>,
  onSubmit: CallableFunction,
  cancelModalHandler: CallableFunction
}

const FBinanceCancelOrderModal = ({showModal, order, ticker, onSubmit, cancelModalHandler}: FBinanceOrderModalProps) => {
  const {serverTimeDiff} = useSelector((state: RootState) => state.fBinance)
  
  const submitCancelOrder = async () => {
    if (!order) return
    const payload: OrderCancelInput = {
      symbol: order.symbol,
      orderId: order.orderId
    }
    const res = await cancelFutureOrder(serverTimeDiff, payload)
    if (res) onSubmit()
  }
  
  return (
    <Modal show={showModal} onHide={() => cancelModalHandler()}>
      <Modal.Header closeButton><Modal.Title>Cancel Order</Modal.Title></Modal.Header>
      <Modal.Body>
        Are you sure you want to cancel <code>{order?.symbol}</code>
        (<code>{order?.type}({order?.stopPrice})</code>)
        ?
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => cancelModalHandler()} size="sm">Close</Button>
        <Button variant="success" onClick={submitCancelOrder} size="sm">Save</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default FBinanceCancelOrderModal
