import {FBOrder, FBSymbolTickerStream} from "../service/model/fBinance";
import React, {useState} from "react";
import {Button, Table} from "react-bootstrap";
import moment from "moment";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSync} from "@fortawesome/free-solid-svg-icons";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../app/rootReducer";
import {fetchFBinanceAccountInfo, fetchFBinanceOpenOrders} from "../service/features/fBinanceSlice";
import FBinanceOrderModal from "./modals/FBinanceOrderModal";
import FBinanceCancelOrderModal from "./modals/FBinanceCancelOrderModal";

interface FBinanceOrdersProps {
  orders: Array<FBOrder>
  ticker: Array<FBSymbolTickerStream>
  isLoading: boolean
  refreshFunction?: CallableFunction
}

const FBinanceOrders = ({orders, ticker, isLoading, refreshFunction}: FBinanceOrdersProps) => {
  const columns: Object = {
    symbol: (v: string) => v,
    status: (v: string) => v,
    side: (v: string) => v,
    closePosition: (v: boolean) => v.toString(),
    price: (v: string) => v,
    stopPrice: (v: string) => v,
    type: (v: string) => v,
    time: (v: Date) => moment(v).format('YYYY-MM-DD HH:mm:ss')
  }
  
  const ordersTemp = orders.length ? <>
    <div>Entries: {orders?.length}</div>
    <Table striped bordered size="sm">
      <thead>
      <tr>
        {Object.keys(columns).map(e => <th key={e}>{e}</th>)}
        <td>Action</td>
      </tr>
      </thead>
      <tbody>
      {orders.map((e: FBOrder, idx: number) => <tr key={idx}>
        {
          Object.keys(columns).map((k: string) =>
            <td key={k}>
              { // @ts-ignore
                columns[k](e[k])
              }
            </td>)
        }
        <td>
          <Button variant="secondary" onClick={() => handleCancelOrderModalShow(e)} size="sm">Cancel</Button>
        </td>
      </tr>)}
      </tbody>
    </Table>
  </> : "There are no open orders."
  
  // Modals
  const dispatch = useDispatch()
  const fetchOpenOrders = () => dispatch(fetchFBinanceOpenOrders(serverTimeDiff))
  const fetchAccountInfo = () =>  dispatch(fetchFBinanceAccountInfo(serverTimeDiff))
  
  const [showNewOrderModal, setShowNewOrderModal] = useState<boolean>(false)
  const [showCancelOrderModal, setShowCancelOrderModal] = useState<boolean>(false)
  const [selectedOrder, setSelectedOrder] = useState<FBOrder | null>(null)
  const {serverTimeDiff} = useSelector((state: RootState) => state.fBinance)
  
  const handleCancelOrderModalShow = (o: FBOrder) => {
    setSelectedOrder(o)
    setShowCancelOrderModal(true)
  }
  const handleCancelOrderModalClose = () => {
    setSelectedOrder(null)
    setShowCancelOrderModal(false)
  }
  const onCancelModalSubmit = async () => {
    await fetchOpenOrders()
    handleCancelOrderModalClose()
  }
  
  const handleNewOrderModalClose = () => {setShowNewOrderModal(false)}
  const handleNewOrderModalShow = async () => {setShowNewOrderModal(true)}
  const onOrderModalSubmit = async () => {
    await fetchOpenOrders()
    await fetchAccountInfo()
    handleNewOrderModalClose()
  }
  
  return (
    <>
      <div style={{display: "flex", alignItems: "center"}}>
        <h1>Open Orders</h1>
        <Button size="sm" variant="secondary" onClick={handleNewOrderModalShow}
                style={{margin: "0 10px"}}>Create</Button>
        {refreshFunction ? <FontAwesomeIcon icon={faSync} onClick={() => refreshFunction()}
                                            style={{marginLeft: '5px', cursor: "pointer"}}/> : void(0)}
        
      </div>
      {isLoading ? "Loading..." : ordersTemp}
      <FBinanceCancelOrderModal showModal={showCancelOrderModal}
                                order={selectedOrder}
                                ticker={ticker}
                                onSubmit={onCancelModalSubmit}
                                cancelModalHandler={handleCancelOrderModalClose}/>
      <FBinanceOrderModal showModal={showNewOrderModal}
                          onSubmit={onOrderModalSubmit}
                          ticker={ticker}
                          cancelModalHandler={handleNewOrderModalClose}/>
    </>
  )
}

export default FBinanceOrders
