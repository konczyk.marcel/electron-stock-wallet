import React, {useEffect, useState} from 'react'
import {Table} from 'react-bootstrap'
import moment from "moment"
import {FBIncomeType, FBTransaction} from "../service/model/FBinance"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSync} from "@fortawesome/free-solid-svg-icons";

interface FBinanceTransactionsProps {
  transactions: Array<FBTransaction>
  isLoading: boolean
  refreshFunction?: CallableFunction
}

const FBinanceTransactions = ({transactions, isLoading, refreshFunction}: FBinanceTransactionsProps) => {
  const [income, setIncome] = useState<Map<FBIncomeType, number>>()
  const [filteredTransactions, setFilteredTransactions] = useState<Array<FBTransaction>>([])
  
  useEffect(() => {
    setFilteredTransactions([...transactions.filter(e => Math.abs(+e.income) >= 0.1)])
  }, [transactions])
  
  useEffect(() => {
    setIncome(calculateIncome(transactions))
  }, [transactions])
  
  const calculateIncome = (t: Array<FBTransaction>): Map<FBIncomeType, number> =>
    t.reduce((sum, e) => {
      sum.set(e.incomeType, (sum?.get(e.incomeType) || 0) + +e.income)
      return sum
    }, new Map<FBIncomeType, number>())
  
  const calculateIncomeTotal = (value: Map<FBIncomeType, number>) =>
    Array.from(value).reduce((sum, [k, v]) => sum += v, 0).toFixed(2)
  
  const columns: Object = {
    symbol: (v: string) => v,
    incomeType: (v: FBIncomeType) => v,
    income: (v: string) => <span className={+v >= 0 ? 'text-green' : 'text-red'}>{ (+v).toFixed(2) }</span>,
    asset: (v: string) => v,
    time: (v: Date) => moment(v).format('YYYY-MM-DD HH:mm:ss')
  }
  
  const summaryTemp = income ? <div>
    <div>TOTAL: {calculateIncomeTotal(income)}</div>
    <div>{
      Array.from(income)
        .map(([k, v]) =>
          <div key={k}>
            {k}: {income?.get(k)?.toFixed(2) || 0}
          </div>
        )
    }</div>
    <div>Quantity: {transactions?.length}</div>
  </div> : "No income info."
  
  const transactionsTemp = filteredTransactions?.length > 0 ?
    <>
      <div>Entries: {filteredTransactions?.length}</div>
      <Table striped bordered size="sm">
        <thead>
        <tr>
          {Object.keys(columns).map(e => <th key={e}>{e}</th>)}
        </tr>
        </thead>
        <tbody>
        {filteredTransactions.map((e: FBTransaction, idx: number) => <tr key={idx}>
          {
            Object.keys(columns).map((k: string) =>
              <td key={k}>
                { // @ts-ignore
                  columns[k](e[k])
                }
              </td>)
          }
        </tr>)}
        </tbody>
      </Table>
    </>
    : "No transactions."
  
  return (
    <>
      <div style={{display: "flex", alignItems: "center"}}>
        <h1>Filtered Transactions</h1>
        {refreshFunction ? <FontAwesomeIcon icon={faSync} onClick={() => refreshFunction()}
                                            style={{marginLeft: '5px', cursor: "pointer"}}/> : void(0)}
      </div>
      {isLoading ? "Loading..." : transactionsTemp}
      <h1>Transactions Summary</h1>
      {isLoading ? "Loading..." : summaryTemp}
    </>
  )
}

export default FBinanceTransactions
