import {FBBalance, FBOrder, FBOrderType, FBPosition, FBSymbolTickerStream} from "../service/model/fBinance";
import React, {useState} from "react";
import {Button, Col, Form, Modal, Row, Table} from "react-bootstrap";
import OrderInput, {OrderInputSide, OrderInputType} from "../service/model/binance/OrderInput";
import {cancelAllFutureOrders, createNewFutureOrder} from "../service/api/fBinanceApi";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../app/rootReducer";
import {fetchFBinanceAccountInfo, fetchFBinanceOpenOrders} from "../service/features/fBinanceSlice";

interface FBinancePositionsProps {
  account: FBBalance | null
  orders: Array<FBOrder>
  ticker: Array<FBSymbolTickerStream>
  isLoading: boolean
}

const FBinancePositions = ({account, orders, isLoading, ticker}: FBinancePositionsProps) => {
  const [showCpModal, setShowCpModal] = useState<boolean>(false)
  const [showSlTpModal, setShowSlTpModal] = useState<boolean>(false)
  const {serverTimeDiff} = useSelector((state: RootState) => state.fBinance)
  const [selectedPosition, setSelectedPosition] = useState<FBPosition | null>(null)
  const [slPrice, setSlPrice] = useState<number>(0)
  const [tpPrice, setTpPrice] = useState<number>(0)
  
  const dispatch = useDispatch()
  const fetchOpenOrders = () => dispatch(fetchFBinanceOpenOrders(serverTimeDiff))
  const fetchAccountInfo = () =>  dispatch(fetchFBinanceAccountInfo(serverTimeDiff))
  
  const calculatePNL = (initialValue: string, amount: string, currValue: string | undefined): string => {
    if (!initialValue || !amount || typeof currValue === "undefined") return '-'
    const res = (+currValue - +initialValue) * +amount
    return res.toFixed(2)
  }
  
  const calculatePNLPercentage = (initialValue: string, amount: string, currValue: string | undefined, initialMargin: string): string => {
    if (!initialValue || !amount || typeof currValue === "undefined") return '-'
    const pnl = calculatePNL(initialValue, amount, currValue)
    const res = (((+initialMargin + +pnl) / +initialMargin) * 100) - 100
    return res.toFixed(2)
  }
  
  const positions = account?.positions?.filter(e => +e.entryPrice > 0)
  
  const findSymbolTickerPrice = (symbol: string) => ticker?.find(e => e.s === symbol)?.c || '-'
  
  const findOrdersSL = (symbol: string) =>  orders.find(o => o.symbol === symbol && o.type === FBOrderType.STOP_MARKET && o.closePosition)?.stopPrice
  const findOrdersTP = (symbol: string) =>  orders.find(o => o.symbol === symbol && o.type === FBOrderType.TAKE_PROFIT_MARKET && o.closePosition)?.stopPrice
  
  const positionsTemp = positions?.length ? <>
    <div>Entries: {positions?.length}</div>
    <Table striped bordered size="sm">
      <thead>
      <tr>
        <th key="symbol">Symbol</th>
        <th key="pnl">PnL</th>
        <th key="currentPrice">Current</th>
        <th key="entryPrice">Entry</th>
        <th key="initialMargin">In. Margin</th>
        <th key="qty">Qty</th>
        <th key="leverage">Lvg.</th>
        <th key="isolated">Isolated</th>
        <th key="SL">SL</th>
        <th key="TP">TP</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      {positions.map((e: FBPosition, idx: number) =>
        <tr key={idx}>
          <td>{e.symbol}</td>
          <td className={+calculatePNL(e.entryPrice, e.positionAmt, findSymbolTickerPrice(e.symbol)) >= 0 ? 'text-green' : 'text-red'}>
            <span>{calculatePNL(e.entryPrice, e.positionAmt, findSymbolTickerPrice(e.symbol))} </span>
            <span>({calculatePNLPercentage(e.entryPrice, e.positionAmt, findSymbolTickerPrice(e.symbol), e.initialMargin)}%)</span>
          </td>
          <td>{findSymbolTickerPrice(e.symbol) || '-'}</td>
          <td>{e.entryPrice}</td>
          <td>{(+e.initialMargin).toFixed(2)}</td>
          <td>{e.positionAmt}</td>
          <td>{e.leverage}x</td>
          <td>{e.isolated.toString()}</td>
          <td>
            {findOrdersSL(e.symbol) || '-'} / <span className={+calculatePNL(e.entryPrice, e.positionAmt, findOrdersSL(e.symbol)) >= 0 ? 'text-green' : 'text-red' }>
              {calculatePNL(e.entryPrice, e.positionAmt, findOrdersSL(e.symbol))} ({calculatePNLPercentage(e.entryPrice, e.positionAmt, findOrdersSL(e.symbol), e.initialMargin)}%)
            </span>
          </td>
          <td>{findOrdersTP(e.symbol) || '-'} / <span className={+calculatePNL(e.entryPrice, e.positionAmt, findOrdersTP(e.symbol)) >= 0 ? 'text-green' : 'text-red' }>
              {calculatePNL(e.entryPrice, e.positionAmt, findOrdersTP(e.symbol))} ({calculatePNLPercentage(e.entryPrice, e.positionAmt, findOrdersTP(e.symbol), e.initialMargin)}%)
            </span>
          </td>
          <td>
            <Button variant="secondary" onClick={() => handleCpModalShow(e)} size="sm">Close</Button>
            <Button variant="secondary" onClick={() => handleSlTpModalShow(e)} size="sm"
                    style={{marginLeft: "10px"}}>SL/TP</Button>
          </td>
        </tr>)}
      </tbody>
    </Table>
  </> : "There are no positions."
  
  const handleCpModalClose = () => {
    setSelectedPosition(null)
    setShowCpModal(false)
  }
  const handleCpModalShow = (p: FBPosition) => {
    setSelectedPosition(p)
    setShowCpModal(true)
  }
  
  const handleSlTpModalClose = () => {
    setSelectedPosition(null)
    setShowSlTpModal(false)
  }
  const handleSlTpModalShow = (p: FBPosition) => {
    setSelectedPosition(p)
    setShowSlTpModal(true)
  }
  
  const submitClosePosition = async () => {
    const p = selectedPosition
    if (!p) return
    const payload: OrderInput = {
      symbol: p.symbol,
      side: +p.positionAmt <= 0 ? OrderInputSide.BUY : OrderInputSide.SELL,
      type: OrderInputType.MARKET,
      quantity: Math.abs(+p.positionAmt)
    }
    const res = await createNewFutureOrder(serverTimeDiff, payload)
    const cancelTlSlRes = await cancelAllFutureOrders(serverTimeDiff, payload.symbol)
    if (res && cancelTlSlRes) {
      await fetchOpenOrders()
      await fetchAccountInfo()
      handleCpModalClose()
    }
  }
  
  const submitStopLossTakeProfit = async () => {
    const p = selectedPosition
    if (!p || !tpPrice || !slPrice) return
    const tpPayload: OrderInput = {
      symbol: p.symbol,
      side: +p.positionAmt <= 0 ? OrderInputSide.BUY : OrderInputSide.SELL,
      type: OrderInputType.TAKE_PROFIT_MARKET,
      stopPrice: tpPrice,
      closePosition: true
    }
    const slPayload: OrderInput = {
      symbol: p.symbol,
      side: +p.positionAmt <= 0 ? OrderInputSide.BUY : OrderInputSide.SELL,
      type: OrderInputType.STOP_MARKET,
      stopPrice: slPrice,
      closePosition: true
    }
    const cancelRes = await cancelAllFutureOrders(serverTimeDiff, p.symbol)
    if (cancelRes) {
      const tpRes = await createNewFutureOrder(serverTimeDiff, tpPayload)
      const slRes = await createNewFutureOrder(serverTimeDiff, slPayload)
      if (tpRes && slRes) {
        setSelectedPosition(null)
        setTpPrice(0)
        setSlPrice(0)
        await fetchOpenOrders()
        await fetchAccountInfo()
        handleSlTpModalClose()
      }
    }
  }
  
  const handleSlPriceChange = (e: any) => {
    setSlPrice(e.target.value)
  }
  
  const handleTpPriceChange = (e: any) => {
    setTpPrice(e.target.value)
  }
  
  const closePositionModalTemp = <>
    <Modal show={showCpModal} onHide={handleCpModalClose}>
      <Modal.Header closeButton><Modal.Title>Close Order</Modal.Title></Modal.Header>
      <Modal.Body>
        Are you sure you want to close <code>{selectedPosition?.symbol}</code>
        (<code>{calculatePNL(selectedPosition?.entryPrice || "", selectedPosition?.positionAmt || "", findSymbolTickerPrice(selectedPosition?.symbol || ""))}</code>)
        ?
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleCpModalClose} size="sm">Close</Button>
        <Button variant="success" onClick={submitClosePosition} size="sm">Save</Button>
      </Modal.Footer>
    </Modal>
  </>
  
  const slTpModalTemp = <>
    <Modal show={showSlTpModal} onHide={handleSlTpModalClose}>
      <Modal.Header closeButton><Modal.Title>Change SL/TP</Modal.Title></Modal.Header>
      <Modal.Body>
        <h4>Current</h4>
        <Row>
          <Col sm="6">Price: <code>{findSymbolTickerPrice(selectedPosition?.symbol || "")}</code></Col>
          <Col sm="6">PnL: <code>{calculatePNL(selectedPosition?.entryPrice || "", selectedPosition?.positionAmt || "", findSymbolTickerPrice(selectedPosition?.symbol || ""))}</code></Col>
        </Row>
        <Form as={Row}>
          <Col sm="12"><h4>Take Profit</h4></Col>
          <Col sm="6"><Form.Control type="number" placeholder="Take Profit price" value={tpPrice} onChange={(e) => handleTpPriceChange(e)}/></Col>
          <Col sm="6" style={{display: 'flex', alignItems: "center"}}>
            Estimated PnL:
            <code>{calculatePNL(selectedPosition?.entryPrice || "", selectedPosition?.positionAmt || "", tpPrice?.toString() )}</code>
          </Col>
        </Form>
        <Form as={Row}>
          <Col sm="12"><h4>Stop Loss</h4></Col>
          <Col sm="6"><Form.Control type="number" placeholder="Stop Loss price" value={slPrice} onChange={(e) => handleSlPriceChange(e)}/></Col>
          <Col sm="6">Estimated PnL:
            <code>{calculatePNL(selectedPosition?.entryPrice || "", selectedPosition?.positionAmt || "", slPrice?.toString() )}</code>
          </Col>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleSlTpModalClose} size="sm">Close</Button>
        <Button variant="success" onClick={submitStopLossTakeProfit} size="sm">Save</Button>
      </Modal.Footer>
    </Modal>
  </>
  
  return (
    <>
      <h1>Positions</h1>
      {isLoading ? "Loading..." : positionsTemp}
      {closePositionModalTemp}
      {slTpModalTemp}
    </>
  )
}

export default FBinancePositions
