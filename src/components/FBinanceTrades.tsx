import {FBAccountTrade} from "../service/model/fBinance";
import React, {useEffect, useState} from "react";
import {Table} from "react-bootstrap";
import moment from "moment";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSync} from "@fortawesome/free-solid-svg-icons";

interface FBinanceTradesProps {
  trades: Array<FBAccountTrade>
  isLoading: boolean
  refreshFunction: CallableFunction
}

const FBinanceTrades = ({trades, isLoading, refreshFunction}: FBinanceTradesProps) => {
  const [filteredTrades, setFilteredTrades] = useState<Array<FBAccountTrade>>([])
  
  useEffect(() => {
    setFilteredTrades([...trades.filter(e => Math.abs(+e.realizedPnl) >= 0.1)])
  }, [trades])
  
  const columns: Object = {
    symbol: (v: string) => v,
    side: (v: string) => v,
    price: (v: string) => v,
    qty: (v: string) => v,
    realizedPnl: (v: string) => (+v).toFixed(2),
    commission: (v: string) => (+v).toFixed(2),
    time: (v: Date) => moment(v).format('YYYY-MM-DD HH:mm:ss')
  }
  
  const tradesTemp = filteredTrades.length ? <>
    <div>Entries: {filteredTrades?.length}</div>
    <Table striped bordered size="sm">
      <thead>
      <tr>
        {Object.keys(columns).map(e => <th key={e}>{e}</th>)}
      </tr>
      </thead>
      <tbody>
      {filteredTrades.map((e: FBAccountTrade, idx: number) => <tr key={idx}>
        {
          Object.keys(columns).map((k: string) =>
            <td key={k}>
              { // @ts-ignore
                columns[k](e[k])
              }
            </td>)
        }
      </tr>)}
      </tbody>
    </Table>
  </> : "There are no trades."
  
  return (
    <>
      <div style={{display: "flex", alignItems: "center"}}>
        <h1>Filtered Trades</h1>
        <FontAwesomeIcon icon={faSync} onClick={() => refreshFunction()} style={{marginLeft: '5px', cursor: "pointer"}}/>
      </div>
      {isLoading ? "Loading..." : tradesTemp}
    </>
  )
}

export default FBinanceTrades
