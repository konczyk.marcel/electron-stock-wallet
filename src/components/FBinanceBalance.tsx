import {FBBalance} from "../service/model/FBinance";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faSync } from '@fortawesome/free-solid-svg-icons'

interface FBinanceBalanceProps {
  account: FBBalance | null
  isLoading: boolean
  refreshFunction?: CallableFunction
}

const FBinanceBalance = ({ account, isLoading, refreshFunction } : FBinanceBalanceProps) => {
  
  const accountTemp = account ?
    <>
      <div>Total margin balance: {(+account.totalMarginBalance)?.toFixed(2)}</div>
      <div>Wallet balance: {(+account.totalWalletBalance)?.toFixed(2)}</div>
      <div>Unrealized profit:
        <span className={+account.totalUnrealizedProfit >= 0 ? 'text-green' : 'text-red'}> {(+account.totalUnrealizedProfit)?.toFixed(2)}</span>
      </div>
      <div>Available balance: {(+account.availableBalance)?.toFixed(2)}</div>
    </>
    : "No data about balance."
  
  return (
    <>
      <div style={{display: "flex", alignItems: "center"}}>
        <h1>Balance</h1>
        {refreshFunction ? <FontAwesomeIcon icon={faSync} onClick={() => refreshFunction()}
                                            style={{marginLeft: '5px', cursor: "pointer"}}/> : void(0)}
      </div>
      {isLoading ? "Loading..." : accountTemp}
    </>
  )
}

export default FBinanceBalance
