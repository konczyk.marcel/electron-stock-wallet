import {configureStore, Action} from '@reduxjs/toolkit'
import {ThunkAction} from 'redux-thunk'

import rootReducer, {RootState} from './rootReducer'
import {createFBinanceMarketWSMiddleware} from "./middlewares/fBinanceMarketWSMiddleware";

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(createFBinanceMarketWSMiddleware),
})

export type AppDispatch = typeof store.dispatch

export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>

export default store
