import {BrowserRouter, Link, Redirect, Route, Switch} from "react-router-dom";
import Greetings from "../components/Greetings";
import FBinanceDashboard from "../views/FBinanceDashboard";
import React from "react";
import FBinanceSymbol from "../views/FBinanceSymbol";
import {Nav} from "react-bootstrap";

const App = () => {
  return (
    <div>
      <BrowserRouter basename="/">
        <div>
          <Nav>
            <Nav.Item as="li">
              <Nav.Link to="/settings" as={Link}>Home</Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
              <Nav.Link to="/fb-dashboard" as={Link}>FB Dashboard</Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
              <Nav.Link to="/fb-symbol/sushiusdt" as={Link}>SUSHIUSDT</Nav.Link>
            </Nav.Item>
          </Nav>
        
          <Switch>
            <Route path="/greetings">
              <Greetings/>
            </Route>
            <Route path="/fb-dashboard">
              <FBinanceDashboard/>
            </Route>
            <Route path="/fb-symbol/:symbol">
              <FBinanceSymbol />
            </Route>
            <Redirect from="*" to="/greetings"/>
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  )
}

export default App
