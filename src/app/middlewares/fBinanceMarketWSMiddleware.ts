import {AnyAction, Dispatch, MiddlewareAPI} from "redux";
import {setStream} from '../../service/features/wsFBinanceSlice'

const SEND_FBINANCE_WS_MESSAGE = 'SEND_FBINANCE_WS_MESSAGE'

const createFBinanceMarketWSMiddleware = function (storeApi: MiddlewareAPI) {
  const socket = createFBinanceWS('wss://fstream.binance.com/stream')
  
  socket.onmessage = (e: WebSocketMessageEvent) => {
    const msg = JSON.parse(e.data)
    if (!msg.stream) return
    storeApi.dispatch(setStream(msg))
  };
  
  return (next: Dispatch) => (action: AnyAction) => {
    if (action.type === SEND_FBINANCE_WS_MESSAGE) {
      waitForSocketConnection(socket, () => {
        socket.send(JSON.stringify(action.payload))
      })
      return
    }
    return next(action);
  }
}

const createFBinanceWS = (url: string): WebSocket => {
  const ws = new WebSocket(url)
  ws.onopen = () => console.log("fbws opened")
  ws.onclose = () => console.log("fbws closed")
  return ws
}

const sendFBinanceSocketMsg = (message: FBinanceWSMsg) => ({
  type: SEND_FBINANCE_WS_MESSAGE,
  payload: message
})

function waitForSocketConnection(socket: WebSocket, callback: CallableFunction) {
  setTimeout(
    function () {
      if (socket.readyState === 1) {
        if (callback != null) {
          callback();
        }
      } else {
        console.log("waiting for fbws connection...")
        waitForSocketConnection(socket, callback);
      }
    }, 100)
}

interface FBinanceWSMsg {
  method: FBinanceWSMsgMethod
  params: Array<string>
  id: number
}

enum FBinanceWSMsgMethod {
  SUBSCRIBE = 'SUBSCRIBE',
  UNSUBSCRIBE = 'UNSUBSCRIBE',
  LIST_SUBSCRIPTIONS = 'LIST_SUBSCRIPTIONS'
}

export {
  createFBinanceMarketWSMiddleware,
  sendFBinanceSocketMsg,
  FBinanceWSMsg,
  FBinanceWSMsgMethod
}
