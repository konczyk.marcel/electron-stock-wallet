import { combineReducers } from '@reduxjs/toolkit'
import fBinanceReducer from '../service/features/fBinanceSlice'
import wsBinanceReducer from '../service/features/wsFBinanceSlice'
import fbDashboardReducer from '../service/features/fbDashboardSlice'

const rootReducer = combineReducers({
  fBinance: fBinanceReducer,
  wsFBinance: wsBinanceReducer,
  fbDashboard: fbDashboardReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
