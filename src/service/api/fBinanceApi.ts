import axios from "axios";
import {HmacSHA256} from "crypto-js";
import queryString from "querystring";
import {
  FBAccountTrade,
  FBBalance,
  FBCandlestickInput,
  FBIncomeType,
  FBLeverage,
  FBLeverageBrackets,
  FBOrder,
  FBTransaction
} from "../model/fBinance";
import OrderInput, {OrderInputTimeInForce, OrderInputType} from "../model/binance/OrderInput";
import OrderCancelInput from "../model/binance/OrderCancelInput";

const publicKey = process.env.BINANCE_API_KEY || ''
const secretKey = process.env.BINANCE_API_SECRET || ''
const basePath = process.env.NODE_ENV == 'development' ? '/fapi' : 'https://fapi.binance.com/fapi'

/* ------------------------------------------------------------------------------------------- */
/* --------------------------------------- PUBLIC -------------------------------------------- */
/* ------------------------------------------------------------------------------------------- */

// https://binance-docs.github.io/apidocs/futures/en/#test-connectivity
export const fetchFutureApiStatus = async (): Promise<number> => {
  const res = await axios.get(`${basePath}/v1/ping`)
  return res?.status || 0
}

// https://binance-docs.github.io/apidocs/futures/en/#check-server-time
export const fetchServerTime = async (): Promise<number> => {
  const res = await axios.get(`${basePath}/v1/time`)
  return res?.data.serverTime
}

// https://binance-docs.github.io/apidocs/futures/en/#kline-candlestick-data
export const fetchCandlesticksData = async (input: FBCandlestickInput): Promise<Array<any>> => {
  const res = await axios.request<Array<any>>({
    method: 'get',
    url: `${basePath}/v1/klines`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...input
    }
  })
  return res.data
}

/* ------------------------------------------------------------------------------------------- */
/* --------------------------------------- PRIVATE ------------------------------------------- */
/* ------------------------------------------------------------------------------------------- */

// https://binance-docs.github.io/apidocs/futures/en/#get-income-history-user_data
export const fetchFutureTransactions = async (serverTimeDiff: number, type?: FBIncomeType, startTime?: number): Promise<Array<FBTransaction>> => {
  const timestamp = new Date().valueOf() - serverTimeDiff
  const body = {
    startTime: startTime,
    incomeType: type || FBIncomeType.ANY,
    limit: 1000,
    timestamp: timestamp
  }
  const signature = HmacSHA256(queryString.encode(body), secretKey).toString()
  const res = await axios.request<Array<FBTransaction>>({
    method: 'get',
    url: `${basePath}/v1/income`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...body,
      signature: signature
    }
  })
  return res.data.sort((a, b) => b.time - a.time)
}

// https://binance-docs.github.io/apidocs/futures/en/#account-trade-list-user_data
export const fetchFutureUserTrades = async (serverTimeDiff: number, symbol: string, startTime?: number): Promise<Array<FBAccountTrade>> => {
  const timestamp = new Date().valueOf() - serverTimeDiff
  const body = {
    symbol: symbol,
    startTime: startTime,
    limit: 1000,
    timestamp: timestamp
  }
  const signature = HmacSHA256(queryString.encode(body), secretKey).toString()
  const res = await axios.request<Array<FBAccountTrade>>({
    method: 'get',
    url: `${basePath}/v1/userTrades`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...body,
      signature: signature
    }
  })
  return res.data.sort((a, b) => b.time - a.time)
}

// https://binance-docs.github.io/apidocs/futures/en/#current-all-open-orders-user_data
export const fetchFutureOpenOrders = async (serverTimeDiff: number, symbol?: string): Promise<Array<FBOrder>> => {
  const timestamp = new Date().valueOf() - serverTimeDiff
  const body = {
    timestamp: timestamp
  }
  const signature = HmacSHA256(queryString.encode(body), secretKey).toString()
  const res = await axios.request<Array<FBOrder>>({
    method: 'get',
    url: `${basePath}/v1/openOrders`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...body,
      signature: signature
    }
  })
  return res.data.sort((a, b) => b.time - a.time)
}

// https://binance-docs.github.io/apidocs/futures/en/#account-information-v2-user_data
export const fetchFutureAccountInfo = async (serverTimeDiff: number): Promise<FBBalance> => {
  const timestamp = new Date().getTime() - serverTimeDiff
  const body = {
    timestamp: timestamp
  }
  const signature = HmacSHA256(queryString.encode(body), secretKey).toString()
  const res = await axios.request({
    method: 'get',
    url: `${basePath}/v2/account`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...body,
      signature: signature
    }
  })
  return res.data
}

// https://binance-docs.github.io/apidocs/futures/en/#new-order-trade
export const createNewFutureOrder = async (serverTimeDiff: number, order: OrderInput): Promise<FBOrder> => {
  const timestamp = new Date().getTime() - serverTimeDiff
  const body = {
    timestamp: timestamp,
    ...order
  }
  const signature = HmacSHA256(queryString.encode(body), secretKey).toString()
  const res = await axios.request({
    method: 'post',
    url: `${basePath}/v1/order`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...body,
      signature: signature
    }
  })
  return res.data
}

// https://binance-docs.github.io/apidocs/futures/en/#cancel-order-trade
export const cancelFutureOrder = async (serverTimeDiff: number, order: OrderCancelInput): Promise<FBOrder> => {
  const timestamp = new Date().getTime() - serverTimeDiff
  const body = {
    timestamp: timestamp,
    ...order
  }
  const signature = HmacSHA256(queryString.encode(body), secretKey).toString()
  const res = await axios.request({
    method: 'delete',
    url: `${basePath}/v1/order`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...body,
      signature: signature
    }
  })
  return res.data
}

// https://binance-docs.github.io/apidocs/futures/en/#cancel-all-open-orders-trade
export const cancelAllFutureOrders = async (serverTimeDiff: number, symbol: string): Promise<any> => {
  const timestamp = new Date().getTime() - serverTimeDiff
  const body = {
    timestamp: timestamp,
    symbol: symbol
  }
  const signature = HmacSHA256(queryString.encode(body), secretKey).toString()
  const res = await axios.request({
    method: 'delete',
    url: `${basePath}/v1/allOpenOrders`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...body,
      signature: signature
    }
  })
  return res.data
}

// https://binance-docs.github.io/apidocs/futures/en/#notional-and-leverage-brackets-user_data
export const fetchNotionalLeverageBrackets = async (serverTimeDiff: number, symbol?: string): Promise<Array<FBLeverageBrackets> | FBLeverageBrackets> => {
  const timestamp = new Date().valueOf() - serverTimeDiff
  const body = {
    symbol: symbol,
    timestamp: timestamp
  }
  const signature = HmacSHA256(queryString.encode(body), secretKey).toString()
  const res = await axios.request<Array<FBLeverageBrackets> |  FBLeverageBrackets>({
    method: 'get',
    url: `${basePath}/v1/leverageBracket`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...body,
      signature: signature
    }
  })
  return res.data
}

// https://binance-docs.github.io/apidocs/futures/en/#change-initial-leverage-trade
export const updateInitialLeverage = async (serverTimeDiff: number, symbol: string, leverage: number): Promise<FBLeverage> => {
  const timestamp = new Date().valueOf() - serverTimeDiff
  const body = {
    symbol: symbol,
    leverage: leverage,
    timestamp: timestamp
  }
  const signature = HmacSHA256(queryString.encode(body), secretKey).toString()
  const res = await axios.request<FBLeverage>({
    method: 'post',
    url: `${basePath}/v1/leverage`,
    headers: {
      'X-MBX-APIKEY': publicKey
    },
    params: {
      ...body,
      signature: signature
    }
  })
  return res.data
}
