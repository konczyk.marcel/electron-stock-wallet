import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {AppThunk} from "../../app/store";
import {
  fetchFutureAccountInfo,
  fetchFutureOpenOrders,
  fetchFutureTransactions,
  fetchFutureUserTrades, fetchServerTime
} from "../api/fBinanceApi";
import {FBAccountTrade, FBBalance, FBIncomeType, FBOrder, FBTransaction} from "../model/FBinance";

export enum FBinanceLoading {
  account = 'account',
  transactions = 'transactions',
  trades = 'trades',
  openOrders = 'openOrders'
}

interface FBinanceState {
  account: FBBalance | null
  transactions: Array<FBTransaction>
  trades: Array<FBAccountTrade>
  openOrders: Array<FBOrder>
  isLoading: {
    [FBinanceLoading.account]: boolean
    [FBinanceLoading.transactions]: boolean
    [FBinanceLoading.trades]: boolean
    [FBinanceLoading.openOrders]: boolean
  }
  errors: Array<string> | []
  serverTimeDiff: number
}

const fBinanceInitialState: FBinanceState = {
  account: null,
  transactions: [],
  trades: [],
  openOrders: [],
  isLoading: {
    account: false,
    transactions: false,
    trades: false,
    openOrders: false
  },
  errors: [],
  serverTimeDiff: 0
}

const fBinance = createSlice({
  name: 'fBinance',
  initialState: fBinanceInitialState,
  reducers: {
    getFBinanceStart(state: FBinanceState, { payload }: any) {
      state.isLoading[payload as FBinanceLoading] = true
    },
    getFBinanceAccountInfo(state: FBinanceState, { payload }: PayloadAction<FBBalance>) {
      state.account = payload
      state.isLoading[FBinanceLoading.account] = false
      state.errors = [...state.errors].filter(e => !e.startsWith(FBinanceLoading.account))
    },
    getFBinanceTransactions(state: FBinanceState, {payload}: PayloadAction<Array<FBTransaction>>) {
      state.transactions = payload
      state.isLoading[FBinanceLoading.transactions] = false
      state.errors = [...state.errors].filter(e => !e.startsWith(FBinanceLoading.transactions))
    },
    getFBinanceUserTrades(state: FBinanceState, {payload}: PayloadAction<Array<FBAccountTrade>>) {
      state.trades = payload
      state.isLoading[FBinanceLoading.trades] = false
      state.errors = [...state.errors].filter(e => !e.startsWith(FBinanceLoading.trades))
    },
    getFBinanceOpenOrders(state: FBinanceState, {payload}: PayloadAction<Array<FBOrder>>) {
      state.openOrders = payload
      state.isLoading[FBinanceLoading.openOrders] = false
      state.errors = [...state.errors].filter(e => !e.startsWith(FBinanceLoading.openOrders))
    },
    getFBinanceFailure(state: FBinanceState, action: PayloadAction<string>) {
      state.errors = [...state.errors, action.payload]
      const loadingType = action.payload.replace(new RegExp('@.*'), '') as FBinanceLoading
      state.isLoading[loadingType] = false
    },
    getServerTime(state: FBinanceState, {payload}: PayloadAction<number>) {
      state.serverTimeDiff = new Date().valueOf() - payload
      state.errors = [...state.errors].filter(e => !e.startsWith('serverTime@'))
    }
  }
})

export const {
  getFBinanceStart,
  getFBinanceAccountInfo,
  getFBinanceTransactions,
  getFBinanceUserTrades,
  getFBinanceOpenOrders,
  getFBinanceFailure,
  getServerTime
} = fBinance.actions

export default fBinance.reducer

export const fetchFBinanceAccountInfo = (serverTimeDiff: number): AppThunk => async dispatch => {
  try {
    dispatch(getFBinanceStart(FBinanceLoading.account))
    const res = await fetchFutureAccountInfo(serverTimeDiff)
    dispatch(getFBinanceAccountInfo(res))
  } catch (err) {
    dispatch(getFBinanceFailure(FBinanceLoading.account.concat('@').concat(err.toString())))
  }
}

export const fetchFBinanceTransactions = (serverTimeDiff: number, type?: FBIncomeType, startTime?: number): AppThunk => async dispatch => {
  try {
    dispatch(getFBinanceStart(FBinanceLoading.transactions))
    const res = await fetchFutureTransactions(serverTimeDiff, type, startTime)
    dispatch(getFBinanceTransactions(res))
  } catch (err) {
    dispatch(getFBinanceFailure(FBinanceLoading.transactions.concat('@').concat(err.toString())))
  }
}

export const fetchFBinanceUserTrades = (serverTimeDiff: number, symbol: string, startTime?: number): AppThunk => async dispatch => {
  try {
    dispatch(getFBinanceStart(FBinanceLoading.trades))
    const res = await fetchFutureUserTrades(serverTimeDiff, symbol, startTime)
    dispatch(getFBinanceUserTrades(res))
  } catch (err) {
    dispatch(getFBinanceFailure(FBinanceLoading.trades.concat('@').concat(err.toString())))
  }
}

export const fetchFBinanceOpenOrders = (serverTimeDiff: number, symbol?: string): AppThunk => async dispatch => {
  try {
    dispatch(getFBinanceStart(FBinanceLoading.openOrders))
    const res = await fetchFutureOpenOrders(serverTimeDiff, symbol)
    dispatch(getFBinanceOpenOrders(res))
  } catch (err) {
    dispatch(getFBinanceFailure(FBinanceLoading.openOrders.concat('@').concat(err.toString())))
  }
}

export const fetchFBinanceServerTime = (): AppThunk => async dispatch => {
  try {
    const res = await fetchServerTime()
    dispatch(getServerTime(res))
  } catch (err) {
    dispatch(getFBinanceFailure('serverTime@'.concat(err.toString())))
  }
}
