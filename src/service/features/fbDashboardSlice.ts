import {createSlice} from "@reduxjs/toolkit";
import {AppThunk} from "../../app/store";

interface FBDashboardState {
  selectedSymbol: string
}

const fbDashboardInitialState: FBDashboardState = {
  selectedSymbol: 'BTCUSDT'
}

const fbDashboard = createSlice({
  name: 'fbDashboard',
  initialState: fbDashboardInitialState,
  reducers: {
    getSelectedSymbol(state: FBDashboardState, { payload }: any) {
      state.selectedSymbol = payload
    }
  }
})

export const {
  getSelectedSymbol,
} = fbDashboard.actions

export default fbDashboard.reducer

export const setSelectedSymbol = (symbol: string): AppThunk => async dispatch => dispatch(getSelectedSymbol(symbol))
