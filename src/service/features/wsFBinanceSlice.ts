import {createSlice, PayloadAction} from "@reduxjs/toolkit";

interface WsFBinanceState {
  streams: Object
}

interface WsFBinanceStream {
  stream: string,
  data: object
}

const fBinanceInitialState: WsFBinanceState = {
  streams: {}
}

const wsFBinance = createSlice({
  name: 'ws/fBinance',
  initialState: fBinanceInitialState,
  reducers: {
    setStream(state, {payload}: PayloadAction<WsFBinanceStream>) {
      state.streams = {...state.streams, [payload.stream]: payload.data}
    }
  }
})

export const {
  setStream
} = wsFBinance.actions

export default wsFBinance.reducer
