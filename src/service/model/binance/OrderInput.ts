export default interface OrderInput {
  symbol: string,	// required: YES.
  side: OrderInputSide	// required: YES.
  positionSide?: OrderInputPositionSide	// required: NO.	Default BOTH for One-way Mode ; LONG or SHORT for Hedge Mode. It must be sent in Hedge Mode.
  type: OrderInputType	// required: YES.
  timeInForce?: OrderInputTimeInForce	// required: NO.
  quantity?: number	// required: NO.	Cannot be sent with closePosition=true(Close-All)
  reduceOnly?: boolean	// required: NO.	"true" or "false". default "false". Cannot be sent in Hedge Mode; cannot be sent with closePosition=true
  price?: number,	// required: NO.
  newClientOrderId?: string, // required: NO.	A unique id among open orders. Automatically generated if not sent.
  stopPrice?: number	// required: NO.	Used with STOP/STOP_MARKET or TAKE_PROFIT/TAKE_PROFIT_MARKET orders.
  closePosition?: boolean	// required: NO.	true, false；Close-All，used with STOP_MARKET or TAKE_PROFIT_MARKET.
  activationPrice?: number	// required: NO.	Used with TRAILING_STOP_MARKET orders, default as the latest price(supporting different workingType)
  callbackRate?: number	// required: NO.	Used with TRAILING_STOP_MARKET orders, min 0.1, max 5 where 1 for 1%
  workingType?: OrderInputWorkingType	// required: NO.	stopPrice triggered by: "MARK_PRICE", "CONTRACT_PRICE". Default "CONTRACT_PRICE"
  priceProtect?: boolean	// required: NO.	"TRUE" or "FALSE", default "FALSE". Used with STOP/STOP_MARKET or TAKE_PROFIT/TAKE_PROFIT_MARKET orders.
  newOrderRespType?: OrderInputNewOrderRespType	// required: NO.	"ACK", "RESULT", default "ACK"
  recvWindow?: number	// required: NO.
}

export enum OrderInputSide {
  'BUY' = 'BUY',
  'SELL' = 'SELL'
}

export enum OrderInputPositionSide {
  'LONG' = 'LONG',
  'SHORT' = 'SHORT',
  'BOTH' = 'BOTH'
}

export enum OrderInputType {
  'LIMIT' = 'LIMIT', // req: timeInForce, quantity, price
  'MARKET' = 'MARKET', // req: quantity
  'STOP' = 'STOP', // req: quantity, price, stopPrice
  'TAKE_PROFIT' = 'TAKE_PROFIT', // req: quantity, price, stopPrice
  'STOP_MARKET' = 'STOP_MARKET', // req: timeInForce, quantity, price, stopPrice
  'TAKE_PROFIT_MARKET' = 'TAKE_PROFIT_MARKET', // req: stopPrice
  'TRAILING_STOP_MARKET' = 'TRAILING_STOP_MARKET' // req: callbackRate
}

export enum OrderInputTimeInForce {
  'GTC' = 'GTC'
}

export enum OrderInputWorkingType {
  'CONTRACT_PRICE' = 'CONTRACT_PRICE',
  'MARK_PRICE' = 'MARK_PRICE'
}

export enum OrderInputNewOrderRespType {
  'ACK' = 'ACK',
  'RESULT' = 'RESULT'
}
