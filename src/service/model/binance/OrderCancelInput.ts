export default interface OrderCancelInput {
  symbol: string
  orderId?: number // req: orderId or origClientOrderId
  origClientOrderId?: string // req: orderId or origClientOrderId
  recvWindow?: number
}
