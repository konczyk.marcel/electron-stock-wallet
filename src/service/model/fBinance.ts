/*
  Binance Future Transaction
 */
export interface FBTransaction {
  symbol: string,
  incomeType: FBIncomeType,
  income: string,
  asset: string,
  time: number
}

export interface FBAccountTrade {
  buyer: boolean,
  commission: string,
  commissionAsset: string,
  id: number,
  maker: boolean,
  orderId: number,
  price: string,
  qty: string,
  quiteQty: string,
  realizedPnl: string,
  side: string,
  positionSide: string,
  symbol: string,
  time: number
}

export interface FBOrder {
  avgPrice: string,
  clientOrderId: string,
  cumQuote: string,
  executedQty: string,
  orderId: number,
  origQty: string,
  origType: string,
  price: string,
  reduceOnly: boolean,
  side: string,
  positionSide: string,
  status: string,
  stopPrice: string,         // please ignore when order type is TRAILING_STOP_MARKET
  closePosition: boolean,    // if Close-All
  symbol: string,
  time: number,              // order time
  timeInForce: string,
  type: FBOrderType,
  activatePrice: string,     // activation price, only return with TRAILING_STOP_MARKET order
  priceRate: string,         // callback rate, only return with TRAILING_STOP_MARKET order
  updateTime: number,        // update time
  workingType: string,
  priceProtect: boolean      // if conditional order trigger is protected
}

export enum FBOrderType {
  'STOP_MARKET' = 'STOP_MARKET',
  'TAKE_PROFIT_MARKET' = 'TAKE_PROFIT_MARKET'
}

export interface FBBalance {
  totalWalletBalance: string,
  totalUnrealizedProfit: string,
  availableBalance: string,
  totalMarginBalance: string,
  positions: Array<FBPosition>
}

export interface FBPosition {
  symbol: string,
  leverage: string,
  isolated: boolean,
  initialMargin: string,
  unrealizedProfit: string,
  entryPrice: string,
  positionSide: string,
  positionAmt: string
}

export enum FBIncomeType {
  TRANSFER = 'TRANSFER',
  WELCOME_BONUS = 'WELCOME_BONUS',
  REALIZED_PNL = 'REALIZED_PNL',
  FUNDING_FEE = 'FUNDING_FEE',
  COMMISSION = 'COMMISSION',
  INSURANCE_CLEAR = 'INSURANCE_CLEAR',
  ANY = ''
}

export interface FBCandlestickInput {
  symbol: string,
  interval: CandlestickInterval,
  startTime?: number,
  endTime?: number,
  limit?: number // 500-1500
}

export enum CandlestickInterval {
  '1m' = '1m',
  '3m' = '3m',
  '5m' = '5m',
  '15m' = '15m',
  '30m' = '30m',
  '45m' = '45m',
  '1h' = '1h',
  '2h' = '2h',
  '4h' = '4h',
  '1d' = '1d',
  '1w' = '1w',
  '1M' = '1M'
}

export interface FBSymbolTickerStream {
  e: string // Event type                       // e.g. "24hrTicker",
  E: number // Event time                       // e.g. 123456789,
  s: string // Symbol                           // e.g. "BTCUSDT",
  p: string // Price change                     // e.g. "0.0015",
  P: string // Price change percent             // e.g. "250.00",
  w: string // Weighted average price           // e.g. "0.0018",
  c: string // Last price                       // e.g. "0.0025",
  Q: string // Last quantity                    // e.g. "10",
  o: string // Open price                       // e.g. "0.0010",
  h: string // High price                       // e.g. "0.0025",
  l: string // Low price                        // e.g. "0.0010",
  v: string // Total traded base asset volume   // e.g. "10000",
  q: string // Total traded quote asset volume  // e.g. "18",
  O: number // Statistics open time             // e.g. 0,
  C: number // Statistics close time            // e.g. 86400000,
  F: number // First trade ID                   // e.g. 0,
  L: number // Last trade Id                    // e.g. 18150,
  n: number // Total number of trades           // e.g. 18151
}


export interface FBLeverageBrackets {
  symbol: string // e.g "ETHUSDT",
  brackets: Array<FBLeverageBracket>
}

export interface FBLeverageBracket {
  bracket: number // Notianl bracket
  initialLeverage: number // Max initial leverge for this bracket
  notionalCap: number // Cap notional of this bracket
  notionalFloor: number // Notionl threshold of this bracket
  maintMarginRatio: number // Maintenance ratio for this bracket
  cum: number // Fast
}

export interface FBLeverage {
  leverage: number,
  maxNotionalValue: number,
  symbol: string
}
